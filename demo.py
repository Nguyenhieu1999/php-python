import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
data = pd.read_table("http://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-red.csv", sep=";")
data.head()
data.plot(kind="scatter", x="alcohol", y="quality", grid=True)
# plt.show()
#Đào tạo Models
def ols_cost(X, y, theta):
    inner = np.power(((X * theta.T) - y), 2)
    return np.sum(inner) / (2 * len(X))
def gradientDescent(X, y, theta, alpha, iters):
    
    # Xác định ma trận tạm thời cho theta
    temp = np.matrix(np.zeros(theta.shape))
    
    # Số lượng tham số lặp qua
    parameters = int(theta.ravel().shape[1])
    
    # vector chi phí để xem làm thế nào nó tiến triển qua từng bước
    cost = np.zeros(iters + 1)
    cost[0] = ols_cost(X, y, theta)
    
    # Tính toán sai số ở mỗi bước
    for i in range(iters):
        error = (X * theta.T) - y
        
        for j in range(parameters):
            term = np.multiply(error, X[:,j])
            temp[0,j] = theta[0,j] - ((alpha / len(X)) * np.sum(term))
            
        theta = temp
        cost[i + 1] = ols_cost(X, y, theta)
        
    return theta, cost

#Chuẩn bị dữ liệu
X = pd.DataFrame(data['alcohol'])
X['x_0'] = 1
y = data['quality']


X = np.matrix(X.values)
y = np.matrix(y.values).T
theta = np.matrix(np.zeros(shape=[1,X.shape[1]]))

print(X.shape, y.shape, theta.shape)


ols_cost(X, y, theta)
alpha = 0.01
iters = 1000
theta_final, cost = gradientDescent(X, y, theta, alpha, iters)
ols_cost(X, y, theta_final)
plt.figure(figsize=(12,8))
plt.xlabel("Epoch")
plt.ylabel(r"Cost $J(\theta$)")
plt.plot(cost)
plt.title("Hiếu Nguyễn")
plt.show()


x = np.linspace(data.alcohol.min(), data.alcohol.max(), 100)
y_hat = theta_final[0,1] + theta_final[0,0] * x 


fig, ax = plt.subplots(figsize=(12,8))
ax.plot(x, y_hat, 'r', label='Predicted')
ax.scatter(data.alcohol, data.quality, label='Training Data')
ax.legend(loc=2)
ax.set_xlabel('Alcohol')
ax.set_ylabel('Wine Density')
ax.set_title('Predicted Quality Rating vs. Alcohol')
plt.show()


data_z = (data - data.mean()) / data.std()
data_z.describe()


sigma = data.std()
mu = data.mean()
print("Standard Deviations\n", sigma)
print("\nAverages\n", mu)

#Thêm giá trị x_0
data_z['x_0'] = 1

X = data_z.drop(['quality'], axis=1)
y = data_z['quality']

#Kiểm tra kích thước ma trận
X = np.matrix(X.values)
y = np.matrix(y.values).T
theta = np.matrix(np.zeros(shape=[1,X.shape[1]]))
print(X.shape, y.shape, theta.shape)
theta_final, cost = gradientDescent(X, y, theta, alpha, iters)

for i in range(0, theta_final.shape[1] - 1):
    print(data_z.columns[i], theta_final.T[i])
    ols_cost(X, y, theta_final)
    
fig, ax = plt.subplots(figsize=(12,8))
ax.set_xlabel("Epoch")
ax.set_ylabel(r"Cost $J(\theta)$")
ax.plot(cost)
ax.set_title("Training Cost vs. Training Epoch for Multivariate Wine Quality Regression")
plt.show()

# x_hat_z là miền được chuẩn hóa để sử dụng cho dự đoán
x_hat_z = np.ones((100, X.shape[1]))
for i in range(1, x_hat_z.shape[1]):
    x_hat_z[:,i] = np.linspace(X[:,i].min(), X[:,i].max(), 100)

# y_hat_z là các giá trị được dự đoán, chuẩn hóa.
y_hat_z = x_hat_z * theta_final.T


y_hat = y_hat_z * sigma[len(sigma) - 1] + mu[len(mu) - 1]
x_hat = x_hat_z
# Cột đầu tiên là chặn
x_hat[:,0:(len(sigma) - 1)] = x_hat_z[:,0:(len(
            sigma) - 1)] * np.atleast_2d(sigma[0:(
            len(sigma) - 1)]) + np.atleast_2d(mu[0:(
            len(sigma) - 1)])
fig, ax = plt.subplots(figsize=(12,8))
ax.plot(x_hat[:,10], y_hat, 'r', label='Predicted')
ax.scatter(data.alcohol, data.quality, label='Training Data')
ax.set_xlabel("Alcohol")
ax.set_ylabel("Wine Density")
ax.set_title("Wine Quality vs. Alcohol")
plt.show()


# Sử dụng dữ liệu huấn luyện để tính bình phương R
y_hat_2 = np.ravel(X * theta_final.T * sigma[len(sigma) - 1] + mu[len(mu) - 1])
num = np.sum((data.quality - y_hat_2) ** 2)
den = np.sum((data.quality - data.quality.mean()) ** 2)
r_squared = 1 - num / den
r_squared


X = data.drop(['quality'], axis=1)
y = data['quality']
lm = LinearRegression(fit_intercept=True, normalize=True)
lm.fit(X, y)

X_ = np.matrix(X.values)
x_hat_sk = np.zeros((100, X.shape[1]))
for i in range(X.shape[1]):
    x_hat_sk[:,i] = np.linspace(X_[:,i].min(), X_[:,i].max(), 100)
y_hat_sk = lm.predict(x_hat_sk)


fig, ax = plt.subplots(figsize=(12,8))
plt.scatter(data.alcohol, data.quality, label='Training Data')
plt.plot(x_hat_sk[:,10], y_hat_sk, 'r', label='Predicted Sklearn')
plt.xlabel("Alcohol")
plt.ylabel("Quality")
plt.title("Wine Quality vs. Alcohol")
plt.show()

lm.score(X, y)

y_hat_sk2 = lm.predict(X)

plt.figure(figsize=(12,8))
plt.scatter(y, y_hat_sk2, c='b', label='Sci Kit-Learn',
           s=50)
plt.scatter(y, y_hat_2, c='r', label='Custom Function')
plt.ylabel('Predicted Quality $\hat{y_i}$')
plt.xlabel('Quality $y_i$')
plt.legend(loc=2)
plt.grid()
plt.show()